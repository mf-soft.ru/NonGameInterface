#include %A_ScriptDir%\includes\dx9Funcs.ahk
; #include %A_ScriptDir%\inc\SAMP-UDF-Developer.ahk
#include %A_ScriptDir%\includes\interface\elementClass.ahk
#NoEnv
#MaxThreads 20
#MaxThreadsBuffer on

; Button interface class
; Created By FordeD <https://vk.com/the_forded>
; Creator's team - MF-Soft.ru <https://vk.com/mfsoftru>
; Use SAMP-UDF-Developer functions 
; Created 17.05.2018

class Button extends Element {

    ; create element
    __New(key,fn := -1) {
        this.key := key
        this.callback := fn
        return this
    }

    ;
    ; Getters \ Setters
    ;

    getText() {
        return this.content.text
    }
    setText(text) {
        this.content.text := text
        TextSetString(this.content.handle.text,this.content.text)
        return true
    }

    getFont() {
        return this.content.font
    }
    setFont(font) {
        this.content.font := font
        TextUpdate(this.content.handle.text,this.content.font,5,true,false)
        return true
    }

    getColor() {
        return this.content.color.text
    }
    setColor(color) {
        this.content.color := color
        TextSetColor(this.content.handle.text,this.content.color.text)
        return true
    }

    getBoxColor() {
        return this.content.color.box
        }
        setBoxColor(color) {
        this.content.color := color
        BoxSetColor(this.content.handle.box,this.content.color.box)
        return true
    }

    setWidth(width) {
        this.size.width := width
        return true
    }

    ;
    ; Events
    ;

    isHover() {
        return this.events.hover
    }

    ; get resize event
    isDown() {
        return this.events.down
    }


    elementSetHover() {
        addChatMessage(this.content.color.boxHover)
        BoxSetColor(this.content.handle.box,this.content.color.boxHover)
    }

    elementSetDown() {
        addChatMessage("������� �������")
        BoxSetColor(this.content.handle.box,this.content.color.boxDown)
    }

    elementStartCallback() {
        addChatMessage("Callback")
        this.callback.Call(this.getContent())
        return true
    }

    ;
    ; override functions
    ;
    componentGetHandle() {
        return [this.content.handle.box, this.content.handle.text]
        }
        componentDestroy() {
        TextDestroy(this.content.handle.text)
        BoxDestroy(this.content.handle.box)
        this.content.handle.text := -1
        this.content.handle.box := -1
        this.show := false
        this.callback := -1
        return true
    }

    componentSetPos(diffX,diffY) {
        TextSetPos(this.content.handle.box, diffX,  diffY)
        BoxSetPos(this.content.handle.text, diffX-5,  diffY-5)
        return true
    }

    componentSetShow(show) {
        if (this.componentGetHandle()[1] == -1) 
            return false
        BoxSetShown(this.content.handle.box,show)
        TextSetShown(this.content.handle.text,show)
        this.show := show
        return true
    }
    
    ;
    ; Class functions
    ;

    elementCycle(type,mPosX,mPosY) {
        size := this.getSize()
        pos := this.getPos()
        if (( mPosX >= pos.x && mPosY >= pos.y ) && ( mPosX < pos.x+size.width && mPosY <= pos.y+20 )) {
            if (type != true) {
            if (GetKeyState("vk01", "P")) {
                if (!this.isDown()) {
                this.setEventDown(true)
                return true
                }
            } else {
                if (!this.isHover()) {
                this.setEventHover(true)
                return true
                }
            }
            } else {
            if (this.isHover()) {
                this.elementSetHover()
            } else if (this.isDown()) {
                this.elementSetDown()
                this.elementStartCallback()
                addChatMessage("������")
            } 
            this.setBoxColor(this.content.color.box)
            }
        }
        return false
    }

    elementCreate(posX, posY, width, text, boxColor, font, color, show, size) {
        this.content := {"handle": {"text": -1, "box": -1}, "text": -1, "font": -1, "color": {"text": -1, "box": -1, "boxHover": -1, "boxDown": -1}}
        this.pos.x := posX
        this.pos.y := posY
        this.size.width := width
        this.content.text := text
        this.content.font := font
        this.content.color.text := color
        this.content.color.box := boxColor
        this.content.size := size
        hoverColor := hex2rgb(SubStr(boxColor, 4))
        this.content.color.boxHover := "0xFF" rgb2hex(hoverColor[1]+25, hoverColor[2]+25, hoverColor[3]+25, 0)
        this.content.color.boxDown := "0xFF" rgb2hex(hoverColor[1]-25, hoverColor[2]-25, hoverColor[3]-25, 0)
        this.events := Object({"move": false, "resize": false, "hover": false, "down": false})
        this.show := show
        addChatMessage(this.show)
        this.content.handle.text := TextCreate(font, size, true, false, posX+5, posY+5, color, text, true, this.show)
        this.content.handle.box := BoxCreate(posX, posY, width, 20, boxColor, this.show)
    }

    ; Event down
  setEventDown(isDown) {
    this.events.down := isDown
  }

  setEventHover(isHover) {
    this.events.hover := isHover
  }
}
#include %A_ScriptDir%\includes\dx9Funcs.ahk
; #include %A_ScriptDir%\includes\SAMP-UDF-Developer.ahk
#include %A_ScriptDir%\includes\interface\labelClass.ahk
#include %A_ScriptDir%\includes\interface\timerClass.ahk
#include %A_ScriptDir%\includes\interface\buttonClass.ahk
#NoEnv
#MaxThreads 20
#MaxThreadsBuffer on

; Main interface class
; Created By FordeD <https://vk.com/the_forded>
; Creator's team - MF-Soft.ru <https://vk.com/mfsoftru>
; Use SAMP-UDF-Developer functions 
; Created 17.05.2018

class Interface {
  ; main Class parametres
  mainMenu := {}
  content := {}
  pos := {"x": 0, "y": 0}
  size := {"width": 0, "height": 0}
  show := false
  static cursor := false

  events := {"move": false, "resize": false}

  ; init class
  __New()
	{
		this.MainMenu := {"main": { "handle": -1 }, "titleBar": { "handle": -1 }, "title": { "handle": -1 }, "sizer": { "handle": -1 }}
		return this
	}

  ; remove class
  __delete()
	{
		BoxDestroy(this.MainMenu.main.handle)
		BoxDestroy(this.MainMenu.titleBar.handle)
		TextDestroy(this.MainMenu.title.handle)
		;ImageDestroy(this.MainMenu.sizer.handle)
		return true
	}

  ;
  ; Getters && Setters
  ;

  ; return window positions
  getPos() {
		return { "x": this.pos.x, "y": this.pos.y }
	}

  ; return window size
  getSize() {
		return { "width": this.size.width, "height": this.size.height }
	}

  ; set window position
  setPos(x := 0, y := 0) {
    if (x < 0 || y < 0 ) 
      return false

    this.pos.x := x
    this.pos.y := y
    return true
  }

  ; set window size
  setSize(width := 0, height := 0) {
    if (width < 0 || height < 0 ) 
      return false

    this.size.width := width
    this.size.height := height
    return true
  }

  ; return window content elements
  getContent() {
    return this.content
  }

  getContentByKey(key) {
    if (this.content.Length() > 0) {
      for each, elem in this.content
      {
        if (elem.key == key) {
          return elem
        }
      }
    }
  }

  ; return window positions
  getShown() {
		return this.show
	}

  ;
  ; Events
  ;

  ; set move event
  setEventMove(move) {
    this.events.move := move
    return true
  }

  ; set resize event
  setEventResize(resize) {
    this.events.resize := resize
    return true
  }

  ; get move event
  isMove() {
    return this.events.move
  }

  ; get resize event
  isResize() {
    return this.events.resize
  }

  ; Move dialog func
  windowMove() {
    size := this.getSize()
    pos := this.getPos()
    BoxSetPos(this.mainMenu.main.handle, pos.x, pos.y)
		BoxSetPos(this.mainMenu.titleBar.handle, pos.x, pos.y)
		TextSetPos(this.mainMenu.title.handle, pos.x+5, pos.y+5)
		;ImageSetPos(this.mainMenu.sizer.handle, pos.x+size.width-16, PosY+size.hei-16)

    if (this.content.Length() > 0) {
      for each, elem in this.content
      {
        elem.elementMove(pos.x+5,pos.y+5+(each*20))
      }
    }
    return true
  }

  ; Resize dialog func
  windowResize(ResizeX,ResizeY) {
    this.setSize(ResizeX,ResizeY)
    size := this.getSize()
    pos := this.getPos()
    BoxSetWidth(this.mainMenu.main.handle, ResizeX)
    BoxSetHeight(this.mainMenu.main.handle, ResizeY)
		BoxSetWidth(this.mainMenu.titleBar.handle, ResizeX)
		;ImageSetPos(this.mainMenu.sizer.handle, size.width+pos.x-16, size.height+pos.y-16)
    if (this.content.Length() > 0) {
      for each, elem in this.content
      {
        elem.elementResize(ResizeX,ResizeY,pos.x,pos.y)
      }
    }

    return true
  }

  ;
  ; Class Functions
  ;

  elementAdd(type, key, text, size := 5, fn := false, boxColor := 0xFF11B5F4, font := "Arial", color := 0xFFFFFFFF) {
    pos := this.getPos()
    size := this.getSize()
    index := this.content.Length()
    if (type == "label") {
      element := new Label(key)
      posY := (pos.y+(index*20))+20
      element.elementCreate(pos.x, posY, size.width, text, boxColor, font, color, this.getShown(), size)
    } else if (type == "button") {
      element := new Button(key,fn)
      posY := (pos.y+(index*20))+20
      element.elementCreate(pos.x, posY, size.width, text, boxColor, font, color, this.getShown(), size)
    }
    this.content.push(element)
  }

  ; Create window func
  windowCreate(posX,posY,width,height,title, showDialog := false, winColor := 0xCC212121, titleBarColor := 0xEE212121, windowFont := "Arial", titleColor := 0xFFFFFFFF) {
		this.pos.x := posX
		this.pos.y := posY
		this.size.width := width
		this.size.height := height
    this.mainMenu.title.text := title
		this.mainMenu.main.handle := BoxCreate(this.pos.x, this.pos.y, this.size.width, this.size.height, winColor, false)
		this.mainMenu.titleBar.handle := BoxCreate(this.pos.x, this.pos.y, this.size.width, 20, titleBarColor, false)
		this.mainMenu.title.handle := TextCreate(windowFont, 6, true, false, this.pos.x+5, this.pos.y+5, titleColor, title, true, false)
		;this.mainMenu.sizer.handle := ImageCreate("icons\sizer.png", this.pos.x+this.size.width-16, this.pos.y+this.size.height-16, 0,  0, false)
    if (showDialog) {
      return this.dialogShow()
    } else {
      return true
    }
	}

  windowDestroy() {
    BoxDestroy(this.mainMenu.main.handle)
		BoxDestroy(this.mainMenu.titleBar.handle)
		TextDestroy(this.mainMenu.title.handle)
		;ImageDestroy(this.mainMenu.sizer.handle)

    this.mainMenu.main.handle := -1
    this.mainMenu.titleBar.handle := -1
    this.mainMenu.title.handle := -1
    this.mainMenu.sizer.handle := -1

    if (this.content.Length() > 0) {
      for each, elem in this.content
      {
        elem.elementDestroy()
      }
    }

    this.show := false
    this.Stop()
    return true
  }

  ; Show dialog func
  windowShow() {
    if (this.mainMenu.main.handle == -1 || this.mainMenu.titleBar.handle == -1 || this.mainMenu.title.handle == -1) 
      return false
		BoxSetShown(this.mainMenu.main.handle,true)
		BoxSetShown(this.mainMenu.titleBar.handle,true)
		TextSetShown(this.mainMenu.title.handle,true)
		;ImageSetShown(this.mainMenu.sizer.handle,true)

    if (this.content.Length() > 0) {
      for each, elem in this.content
      {
        elem.elementShow()
      }
    }

    this.show := true
    this.Start()
    return true
	}

  ; Hide dialog func
  windowHide() {
    if (this.mainMenu.main.handle == -1 || this.mainMenu.titleBar.handle == -1 || this.mainMenu.title.handle == -1) 
      return false
		BoxSetShown(this.mainMenu.main.handle,false)
		BoxSetShown(this.mainMenu.titleBar.handle,false)
		TextSetShown(this.mainMenu.title.handle,false)
		;ImageSetShown(this.mainMenu.sizer.handle,false)

    if (this.content.Length() > 0) {
      for each, elem in this.content
      {
        elem.elementHide()
      }
    }

    this.show := false
    this.Stop()
    return true
	}

  ; Main Class Cycle
  windowCycle() {
    if (!IsPlayerInMenu()) {
      ; In Cycle class ection
      actionElement := -1
      isElementsAction := false
      GetCoordsSamp(mPosX,mPosY)
      While GetKeyState("vk01", "P") {
        this.priorityChange(1)
        GetCoordsSamp(mPosX,mPosY)
        size := this.getSize()
        pos := this.getPos()
        RasnX := 0
        RasnY := 0
        ResizeX := 0
        ResizeY := 0

        ; Set event
        if (( mPosX >= pos.x && mPosY >= pos.y ) && ( mPosX < pos.x+size.width && mPosY <= pos.y+20 )) {
          if (!this.isMove()) {
            this.setEventMove(true)
          }
          RasnX := mPosX-pos.x
          RasnY := mPosY-pos.y
        } else if (!this.isResize() && (mPosX >= pos.x+size.width-20 && mPosY >= pos.y+size.height-20 && mPosY <= pos.y+size.height+40 && mPosX <= pos.x+size.width+40)) {
          this.setEventResize(true)
        } 

        ; Events Action
        if (this.isMove()) {
          if (mPosX-RasnX != pos.x || mPosY-RasnY != pos.y) {
            newPosX := mPosX-RasnX
            newPosY := mPosY-RasnY
            this.setPos(newPosX,newPosY)
            this.windowMove()
          }
        } else if (this.isResize()) {
          ResizeX := mPosX-pos.x
          ResizeY := mPosY-pos.y
          if ((size.height != ResizeY || size.width != ResizeX)) {
            if (ResizeY < 120 || ResizeX < 100) {
              this.setEventResize(false)
            } else {
              this.windowResize(ResizeX,ResizeY)
            }
          }
        }
        ; On mouse acions elementsCycle
        if (this.content.Length() > 0) {
          for each, elem in this.content
          {
            isElementsAction := elem.elementCycle(false,mPosX,mPosY)
            if(isElementsAction) {
              actionElement := each
              break
            }
          }
        }
        ; Acions elementsCycle
        if(isElementsAction) {
          this.content[actionElement].elementCycle(true,mPosX,mPosY)
        }
      }
      if (this.isMove()) { 
        this.setEventMove(false)
      }
      if (this.isResize()) {
         this.setEventResize(false)
      }
      this.priorityChange(0)
    } else {
      this.dialogHide()
    }
    return true
  }

  ; Timer start checking
  Start(period:=15)
	{
		fn := this.windowCycle.Bind(this)
		this.Timer := new interfaceTimer(fn, period, priority)
		this.Timer.Start()
	}
 
  ; Timer stop checking
	Stop()
	{
		this.Timer.Stop()
	}

  priorityChange(priority := 0)
	{
		this.Timer.prirityChange(priority)
	}

  ; show cursor
  cursorShow() { 
    if(!checkHandles()) 
      return -1 
    byte := readMem(hGTA, dwSAMP + 0x69529, 1, "byte") 
    writeBytes(hGTA, dwSAMP + 0x69529, "909090909090") 
    sleep 50 
    writeBytes(hGTA, dwSAMP + 0x69529, "0F84AE000000") 
    this.cursor := true
    return true
  }

  ; hide cursor
  cursorHide() { 
    if(!checkHandles()) 
      return -1 
    byte := readMem(hGTA, dwSAMP + 0x69529, 0, "byte") 
    writeBytes(hGTA, dwSAMP + 0x69529, "909090909090") 
    sleep 50 
    writeBytes(hGTA, dwSAMP + 0x69529, "0F84AE000000") 
    this.cursor := false
    return true
  }

  ; TODO:
  ; * label +
  ; * button
  ; * line
  ; * input
  ; * comboBox
  ; * scroller
  ; * checkBox

}
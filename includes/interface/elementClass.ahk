#include %A_ScriptDir%\includes\dx9Funcs.ahk
; #include %A_ScriptDir%\includes\SAMP-UDF-Developer.ahk
#NoEnv
#MaxThreads 20
#MaxThreadsBuffer on

; Base element interface class
; Created By FordeD <https://vk.com/the_forded>
; Creator's team - MF-Soft.ru <https://vk.com/mfsoftru>
; Use SAMP-UDF-Developer functions 
; Created 17.05.2018

class Element {
  key := -1
  pos := {"x": 0, "y": 0}
  size := {"width": 0, "height": 15}
  show := false
  content := Object()
  events := Object({"move": false, "resize": false})
  
  ; create element
  __New(key) {
      this.key := key
      return this
  }

  ; remove class
  __delete() {
    this.elementDestroy()
    key := -1
    content := Object()
    events := Object()
    return true
  }

  ;
  ; Getters \ Setters
  ;

  getKey() {
    return this.key
  }
  setKey(key) {
    this.key := key
    return true
  }

  getPos() {
    return this.pos
  }
  setPos(posX,posY) {
    this.pos.x := posX
    this.pos.y := posY
    return true
  }

  getSize() {
    return this.size
  }
  setSize(width,height) {
    this.size.width := width
    this.size.height := height
    return true
  }

  getShown() {
    return this.show
  }

  getContent() {
    return this.content
  }

  ;
  ; Event returns
  ;

  isMove() {
    return this.events.move
  }

  ; get resize event
  isResize() {
    return this.events.resize
  }

  ;
  ; override functions
  ;

  componentGetHandle() {
    return true
  }

  componentDestroy() {
    return true
  }

  componentSetPos(diffX,diffY) {
    return true
  }

  componentSetShow(show) {
    return true
  }

  elementCycle() {
      return false
  }

  ;
  ; Class functions
  ;

  ; Resize dialog func
  elementResize(ResizeX, ResizeY, winPosX, winPosY) {
    pos := this.getPos()
    size := this.getSize()
    if (((winPosY+ResizeY)-5) < pos.y) {
      if (this.getShown()) {
        this.elementHide()
      }
    } else {
      if (!this.getShown()) {
        this.elementShow()
      }
      this.setWidth(ResizeX)
    }
    return true
  }

  elementDestroy() {
    return this.componentDestroy()
  }

  ; Move dialog func
  elementMove(diffX,diffY) {
    return this.componentSetPos(diffX, diffY)
  }

  ; Show dialog func
  elementShow() {
    return this.componentSetShow(true)
  }

  ; Hide dialog func
  elementHide() {
    return this.componentSetShow(false)
  }
}
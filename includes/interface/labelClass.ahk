#include %A_ScriptDir%\includes\dx9Funcs.ahk
; #include %A_ScriptDir%\includes\SAMP-UDF-Developer.ahk
#include %A_ScriptDir%\includes\interface\elementClass.ahk
#NoEnv
#MaxThreads 20
#MaxThreadsBuffer on

; Label interface class
; Created By FordeD <https://vk.com/the_forded>
; Creator's team - MF-Soft.ru <https://vk.com/mfsoftru>
; Use SAMP-UDF-Developer functions 
; Created 17.05.2018

class Label extends Element {
  ;
  ; Getters \ Setters
  ;

  getText() {
    return this.content.text
  }
  setText(text) {
    this.content.text := text
    TextSetString(this.content.handle,this.content.text)
    return true
  }

  getFont() {
    return this.content.font
  }
  setFont(font) {
    this.content.font := font
    TextUpdate(this.content.handle,this.content.font,5,true,false)
    return true
  }

  getColor() {
    return this.content.color
  }
  setColor(color) {
    this.content.color := color
    TextSetColor(this.content.handle,this.content.color)
    return true
  }

  setWidth(width) {
    this.size.width := width
    return true
  }

  ;
  ; override functions
  ;
  componentGetHandle() {
    return this.content.handle
  }
  componentDestroy() {
    TextDestroy(this.content.handle)
    this.content.handle := -1
    this.show := false
    return true
  }

  componentSetPos(diffX,diffY) {
    TextSetPos(this.content.handle, diffX,  diffY)
    return true
  }

  componentSetShow(show) {
    if (this.componentGetHandle() == -1) 
      return false
    TextSetShown(this.content.handle,show)
    this.show := show
    return true
  }
  
  ;
  ; Class functions
  ;

  elementCreate(posX, posY, width, text, boxColor, font, color, show, size) {
    this.content := {"handle": -1, "text": -1, "font": -1, "color": -1}
    this.pos.x := posX
    this.pos.y := posY
    this.size.width := width
    this.content.text := text
    this.content.font := font
    this.content.color := color
    this.show := show
    this.content.size := size
    this.content.handle := TextCreate(font, size, true, false, posX+5, posY+5, color, text, true, this.show)
  }
}
#include %A_ScriptDir%\includes\dx9Funcs.ahk
; #include %A_ScriptDir%\includes\SAMP-UDF-Developer.ahk
#NoEnv
#MaxThreads 20
#MaxThreadsBuffer on

; Timer interface class
; Created By FordeD <https://vk.com/the_forded>
; Creator's team - MF-Soft.ru <https://vk.com/mfsoftru>
; Use SAMP-UDF-Developer functions 
; Created 17.05.2018

; Inner class timer
class interfaceTimer {
  __New(target, period)
  {
    this.Target := target
    this.Period := period
  }
  
  Start()
  {
    SetTimer, % this, % this.Period
  }
  
  Stop()
  {
    SetTimer, % this, Delete
  }

  prirityChange(priority) {
    this.Priority := priority
    SetTimer, % this, % this.Period, % this.Priority
  }
  
  Call()
  {
    this.Target.Call()
  }
}
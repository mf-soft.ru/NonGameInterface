#SingleInstance Force
#Persistent
#include includes\interface\interfaceClass.ahk

SetParam("use_window", "1")
SetParam("window", "GTA:SA:MP")

newWindow := new Interface()

; First window 

!1::
  newWindow.windowCreate(300,200,150,280, "����� Interface")
  newWindow.elementAdd("label", "first", "������ �������")
return

!2::
  if (!newWindow.getShown()) {
    newWindow.windowShow()
  } else {
    newWindow.windowHide()
  }
return

!3::
  newWindow.windowDestroy()
return

; Destroy All Windows && reload script

!4::
  DestroyAllVisual()
  reload
return

; Show\Hide Cursor

!5::
  newWindow.cursorShow()
return

!0::
  newWindow.elementAdd("label", "second", "������ �������")
return

!9::
  newWindow.getContent()[1].setText("����� ������ ������ ���������!")
  newWindow.getContent()[2].setText("����� ������ ������ ��������� ����!")
return